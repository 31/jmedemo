package mygame;

import com.jme3.app.SimpleApplication;
import com.jme3.collision.CollisionResults;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.queue.RenderQueue.ShadowMode;
import com.jme3.scene.CameraNode;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * test
 * @author normenhansen
 */
public class Main extends SimpleApplication implements ActionListener
{
	boolean left, right;
	
	Node player;
	
	CameraNode camNode;
	
	float timeUntilEvilSpawn;
	LinkedList<Geometry> evils;
	
	static final float PADDLE_SPEED = 15f;
	static final float CORRIDOR_WIDTH = 15f;
	
	static final float SPAWN_EVIL_INTERVAL = 2f;
	
	static final float EVIL_FALL_SPEED = 6f;

	public static void main(String[] args)
	{
		Main app = new Main();
		app.start();
	}

	@Override
	public void simpleInitApp()
	{
		// Disable display of debug information.
		super.setDisplayFps(false);
		super.setDisplayStatView(false);
		
		// Add a scene
		Spatial scene = assetManager.loadModel("Scenes/gameScene.j3o");
		rootNode.attachChild(scene);
		
		// Create a player box
		Box b = new Box(Vector3f.ZERO, 1, 1, 1);
		Geometry geom = new Geometry("Box", b);

		Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		mat.setTexture("ColorMap", assetManager.loadTexture("Textures/backsh.jpg"));
		geom.setMaterial(mat);

		player = new Node();
		
		player.attachChild(geom);
		rootNode.attachChild(player);
		
		// Create edge squares.
		Box leftEdge = new Box(new Vector3f(CORRIDOR_WIDTH + 5f + 1f, 0f, 0f), 5f, 500f, 1f);
		Box rightEdge = new Box(new Vector3f(-CORRIDOR_WIDTH - 5f - 1f, 0f, 0f), 5f, 500f, 1f);
		
		Geometry leftGeom = new Geometry("leftEdge", leftEdge);
		Geometry rightGeom = new Geometry("rightEdge", rightEdge);
		
		Material edgeMat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		
		leftGeom.setMaterial(edgeMat);
		rightGeom.setMaterial(edgeMat);
		
		rootNode.attachChild(leftGeom);
		rootNode.attachChild(rightGeom);
		
		// Disable the default camera
		flyCam.setEnabled(false);
		// Move the camera further from the player.
		cam.setLocation(new Vector3f(0f, 7f, 25f));
		
		// Set up evil spawning.
		timeUntilEvilSpawn = SPAWN_EVIL_INTERVAL;
		evils = new LinkedList<Geometry>();
		
		// Set up control system
		left = false;
		right = false;
		
		inputManager.addMapping("Right", new KeyTrigger(KeyInput.KEY_RIGHT));
		inputManager.addMapping("Left", new KeyTrigger(KeyInput.KEY_LEFT));
		inputManager.addListener(this, "Right", "Left");
	}

	@Override
	public void simpleUpdate(float tpf)
	{
		if(right)
			player.move(tpf * PADDLE_SPEED, 0f, 0f);
		if(left)
			player.move(-tpf * PADDLE_SPEED, 0f, 0f);
		
		// Set boundaries for player movement.
		Vector3f playerTranslation = player.getLocalTranslation();
		playerTranslation.x = Math.min(Math.max(playerTranslation.x, -CORRIDOR_WIDTH), CORRIDOR_WIDTH);
		player.setLocalTranslation(playerTranslation);
		
		cam.lookAt(
				player.getLocalTranslation().add(new Vector3f(0f, 3f, 0f)),
				Vector3f.UNIT_Y);
		
		// Spawn evil if necessary.
		timeUntilEvilSpawn -= tpf;
		if(timeUntilEvilSpawn <= 0)
		{
			timeUntilEvilSpawn = SPAWN_EVIL_INTERVAL;
			
			spawnEvil((float)Math.random() * CORRIDOR_WIDTH * 2f - CORRIDOR_WIDTH);
		}
		// Make evil fall.
		Iterator<Geometry> evilIter = evils.iterator();
		while(evilIter.hasNext())
		{
			Geometry g = evilIter.next();
			g.move(0f, -tpf * EVIL_FALL_SPEED, 0f);
			
			// Remove evils that are far away.
			if(g.getWorldTranslation().y < -15f)
			{
				g.removeFromParent();
				evilIter.remove();
			}
			
			// Remove evils that collide with the player.
			int numcol = g.collideWith(
					player.getChild("Box").getWorldBound(),
					new CollisionResults());
			// Remove evil on collision.
			if(numcol != 0)
			{
				g.removeFromParent();
				evilIter.remove();
			}
			
			// Do some random spinning!
			g.rotate(tpf * 5.123f, tpf * 9.123f, tpf * 1.231f);
		}
	}
	
	public void spawnEvil(float x)
	{
		Box evilBox = new Box(.5f, .5f, .5f);
		Geometry evilGeom = new Geometry("evil", evilBox);
		
		evilGeom.setLocalTranslation(new Vector3f(x, 15f, 0f));
		
		Material evilMat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		evilMat.setColor("Color", ColorRGBA.Red);
		
		evilGeom.setMaterial(evilMat);
		
		rootNode.attachChild(evilGeom);
		
		evils.add(evilGeom);
	}

	@Override
	public void simpleRender(RenderManager rm)
	{
		//TODO: add render code
	}

	public void onAction(String name, boolean isPressed, float tpf)
	{
		if(name.equals("Left"))
		{
			left = isPressed;
		}
		else if(name.equals("Right"))
		{
			right = isPressed;
		}
	}
}
